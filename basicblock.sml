structure IntKey =
struct
	type ord_key = int
	val compare = Int.compare
end

structure IntMap = ListMapFn (IntKey:ORD_KEY):ORD_MAP


structure IntSet = ListSetFn(IntKey:ORD_KEY):ORD_SET



datatype graph = Graph of (  int list *  IntSet.set IntMap.map  *  IntSet.set IntMap.map  )

fun isolate [] = []
	|isolate ( x::xs ) = x :: isolate (List.filter(fn y => y <> x) xs)
 

(*datatype adjlist *)

(*going to create a function which adds a new vertices to a graph*)

fun add_member ( xs, d ) = xs @ [d]

fun init_dic ( b, d ) = IntMap.insert ( b, d, IntSet.empty )

fun add_dic_edge ( b, x, y ) = 
	let
		val set = IntMap.lookup ( b, x )
		val new_set = IntSet.add ( set, y )
		val new_dic = IntMap.insert ( b, x, new_set )
	in
		new_dic
	end

	 

fun add_new ( Graph(a,b,c), d ) = 
	let
		val a_new = add_member ( a, d )
		val b_new = init_dic ( b, d )
		val c_new = init_dic ( c, d )
	in
		Graph ( a_new, b_new, c_new )
	end


fun add_edge ( Graph(a,b,c), x, y ) =
	let
	 	val b = add_dic_edge ( b, y, x )
	 	val c = add_dic_edge ( c, x, y )
	 in
	 	Graph ( a, b, c )
	 end 


(*fun suc ( Graph ( a, b, c ), x ) = IntMap.lookup ( c, x )

fun pred ( Graph ( a, b, c ), x ) = IntMap.lookup ( b, x )*)


fun is_member(x,[]) = false
	|is_member(x,y::ls) =
	if ( x = y ) 
	then true 
	else is_member(x,ls)


fun set_difference ( [], ys, k ) = k
	|set_difference ( x::xs, ys, k ) =
	if ( is_member ( x,ys ) ) then set_difference ( xs, ys, k )
	else set_difference ( xs, ys, k @ [x] )



fun find_pred_suc (x,b,c) =
	let
		val pred_num = IntSet.numItems ( IntMap.lookup ( b, x ) )
	 	val pred_list = IntSet.listItems ( IntMap.lookup ( b, x ) )
	 	val suc_num = IntSet.numItems ( IntMap.lookup ( c, x ) )
	 	val suc_list = IntSet.listItems ( IntMap.lookup ( c, x ) )
	in
		(pred_num,pred_list,suc_num,suc_list)
	end



fun check_entry ([], b, c, k ) = k
	|check_entry ( x::xs, b, c, k ) =
	let
		val (pred_num,pred_list,suc_num,suc_list) = find_pred_suc (x,b,c)	 	
	in
		if ( pred_num = 1 andalso suc_num < 2 ) 
	 	then 
	 		if ( is_member ( x, k ) )
	 		then k
	 		else check_entry ( pred_list, b, c, [x] @ k )
	 	else
	 		if ( suc_num > 1 ) then k
	 		else [x] @ k
	end 




fun check_exit ( [], b, c, k ) = k
	|check_exit ( x::xs, b, c, k ) = 
	let
		val (pred_num,pred_list,suc_num,suc_list) = find_pred_suc (x,b,c) 
	in
		if ( suc_num = 1 andalso pred_num < 2 ) 
		then
			if ( is_member ( x, k ) )
			then k @ [x]
			else check_exit ( suc_list, b, c, k @ [x] ) 
 		else
 			if ( pred_num > 1 ) then k
 			else k @ [x] 
	end







fun leader (x, b, c) =
	let
		val (pred_num,pred_list,suc_num,suc_list) = find_pred_suc (x,b,c)
	in
		if ( not (pred_num = 1) andalso not (suc_num = 1) )
		then [x]
		else
			if (pred_num = 1 andalso suc_num = 1)
			then check_entry ( pred_list, b, c, []) @ [x] @ check_exit (suc_list, b, c, []) 
			else
				if (pred_num = 1)
				then check_entry ( pred_list, b, c, []) @ [x]
				else [x] @ check_exit (suc_list, b, c, [])
	end



fun find_basic_node ( [], b, c, ker ) = ker
	|find_basic_node ( x::xs, b, c, ker ) =
	let
		val basic_block =  isolate ( leader (x,b,c) )
		val newlist = set_difference ( xs, basic_block, [] )
		val ker_up = ker @ [basic_block]
	in
		find_basic_node ( newlist, b, c, ker_up )
	end

fun driver ( Graph (a,b,c) ) = find_basic_node (a,b,c, [])

fun empty_graph () = Graph([],IntMap.empty,IntMap.empty)



(*val graph1 = empty_graph()

val graph1 = add_new (graph1, 1)
val graph1 = add_new (graph1, 2)
val graph1 = add_new (graph1, 3)
val graph1 = add_new (graph1, 4)

val graph1 = add_edge (graph1, 1, 2)
val graph1 = add_edge (graph1, 1, 4)
val graph1 = add_edge (graph1, 2, 3)
val graph1 = add_edge (graph1, 2, 4)
val graph1 = add_edge (graph1, 3, 1)*)

(*
val graph1 = empty_graph()
val graph1 = add_new (graph1, 1)
val graph1 = add_new (graph1, 2)
val graph1 = add_new (graph1, 3)
val graph1 = add_new (graph1, 4)
val graph1 = add_new (graph1, 5)
val graph1 = add_new (graph1, 6)

val graph1 = add_edge (graph1, 1, 2)
val graph1 = add_edge (graph1, 2, 3)
val graph1 = add_edge (graph1, 3, 4)
val graph1 = add_edge (graph1, 4, 1)
val graph1 = add_edge (graph1, 2, 5)
val graph1 = add_edge (graph1, 5, 6)
*)


(*
val graph1 = empty_graph()
val graph1 = add_new (graph1, 1)
val graph1 = add_new (graph1, 2)
val graph1 = add_new (graph1, 3)
val graph1 = add_edge (graph1, 1, 2)
val graph1 = add_edge (graph1, 2, 3)
val graph1 = add_edge (graph1, 3, 1)*)

(*val c = driver (graph1)*)
