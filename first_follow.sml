
	(*creating a dictionary function which is necessary to find out the corresponding value of a key*)

	structure StringKey =
	struct
	type ord_key = string
	val compare = String.compare
	end

	structure StringMap = ListMapFn (StringKey:ORD_KEY):ORD_MAP


	(*capturing the structure of a grammar*)

	datatype symbol = Nonterminal of string|Terminal of string;
	datatype rule = Rule of symbol * symbol list;
	datatype grammar = Gram of rule list * symbol;


	(*this is function to check whether an item belongs to list or not*)

	fun is_member(x,[]) = false
		|is_member(x,y::ls) =
		if ( x=y ) 
		then true 
		else is_member(x,ls)


	(*this simply takes a symbol and changes it to string so that we can store it in dictionary as a key*)

	fun str (Nonterminal(x)) = x
		|str (Terminal(x)) = x


	fun areEq([],[]) = true
		| areEq([],ls) = false
		| areEq(ls,[]) = false
		| areEq(a::la,b::lb) =
		if(a=b)
		then areEq(la,lb)
		else false

	fun union([],lb,lc) = lc@lb
		| union(a::la,lb,lc) =
		if(is_member(a,lb))
		then union(la,lb,lc)
		else union(la,lb,lc@[a])


	fun is_same ( [], new_dict, dic ) = true
		|is_same ( x::xs, new_dict, dic ) = 
		let
			val a = StringMap.lookup( dic, str(x) )
			val b = StringMap.lookup( new_dict, str(x) )
		in
			if ( a = b )
			then is_same ( xs, new_dict, dic )
			else false
		end


	fun find_terminal ([],ter) = ter
		  |find_terminal (Terminal(y)::ys,ter) = find_terminal(ys,union(ter,[Terminal(y)],[]))
		  |find_terminal (Nonterminal(y)::ys,ter) = find_terminal(ys,ter)


	fun iter_grammar_ter([],ter) = ter
	  |iter_grammar_ter(Rule(x,y)::rs,ter) = iter_grammar_ter(rs,find_terminal(y,ter))


	(*the function to find out the nonterminal of the grammar*)

	fun create_ntlist ([],k) = k
		|create_ntlist(Rule(x,y)::ls,k) =
		if is_member(x,k)
		then create_ntlist(ls,k)
		else create_ntlist(ls,k@[x])


	(*this function takes a symbol and iterate through the grammar to find out rules associated with it*)

	fun iter_grammar_rule(x,[],rule_list) = rule_list
		|iter_grammar_rule(x,Rule(a,b)::ls,rule_list) =
		if (x=a)
		then iter_grammar_rule(x,ls,rule_list@[b])
		else iter_grammar_rule(x,ls,rule_list)


	(*this driver function takes a grammar and generates a dictionary whose keys are
	non terminals of the grammar and value is the list of associated rules*)
	fun driver_to_find_prod ( a ) =
	let
		val nt_list = create_ntlist(a,[])
	  	val symbol_rule_dict = StringMap.empty
	  	fun iter_nt_rule ([],z) = z
    		|iter_nt_rule (x::xs,z) = iter_nt_rule(xs,StringMap.insert(z,str(x),iter_grammar_rule(x,a,[])))
	in
	  	iter_nt_rule(nt_list,symbol_rule_dict)
	end

	(*this function checks whether two lists are equal or not*)
	(*this will be needed in both first and nullable set construction to indicate where to stop the iteration*)

	fun init_null ([],k) = k
		|init_null (x::xs,k) = init_null(xs, StringMap.insert(k,str(x),false) )


	fun each_prod_null ( s, [], null_dic ) = StringMap.insert( null_dic, str(s), true )
		|each_prod_null ( s, x::xs, null_dic ) = 
				if ( StringMap.lookup( null_dic, str(x) ) )
				then each_prod_null( s, xs, null_dic)
				else StringMap.insert( null_dic, str(s), false )

	fun iter_prod_list_null ( x, [], null_dic) = null_dic
		|iter_prod_list_null( x, y::ys, null_dic ) = 
		let
			val new_dict = each_prod_null ( x, y, null_dic)
		in
			iter_prod_list_null( x, ys, new_dict ) 
		end

	fun find_null ([],prod_dic,null_dic) = null_dic
		|find_null (x::xs,prod_dic,null_dic) = 
		let
			val prod_list = StringMap.lookup(prod_dic,str(x))
		in
			find_null( xs, prod_dic, iter_prod_list_null ( x, prod_list, null_dic ) )
		end	




	(*this function iter the grammar again and again to update the first dictionary*)
	fun update_null_dic(nt_list,prod_dic,null_dic) =
		let
		 	val new_dict = find_null ( nt_list,prod_dic,null_dic )
		 in
			if is_same(nt_list,new_dict,null_dic)
			then new_dict
			else update_null_dic(nt_list,prod_dic,new_dict)
		 end

	fun driver_to_find_null ( Gram( a, b) ) =
		let
				val ter = iter_grammar_ter(a,[])
				val nt_list = create_ntlist(a,[])
				val symbol_rule_dict = driver_to_find_prod (a)
				val symbol_list = union( nt_list, ter , [])
				val null_dic = init_null( symbol_list, StringMap.empty )
			in
				update_null_dic(nt_list,symbol_rule_dict,null_dic)
			end	

(*fun check(Gram(a,b),f) = fn (x) => f(a,x)

val ter = check (grammar1,iter_grammar_ter) []
val null = init_null(nt_list,StringMap.empty)*)
val null = driver_to_find_null(grammar1);



	fun init_terminal([],first) = first
	  |init_terminal(x::xs,first) = init_terminal(xs,StringMap.insert(first,str(x),[x]))

	(*it initialize the non terminal with empty first set*)     
	fun init_non_terminal([],first) = first
			|init_non_terminal(x::xs,first) = init_non_terminal(xs,StringMap.insert(first,str(x),[]))

	(*it takes one production and checks each symbols on rhs for finding first of lhs*)



	fun each_prod(s,[],first_dic) = first_dic
	  |each_prod(s,x::xs,first_dic) =

	  let
	  	val first_set = StringMap.lookup(first_dic,str(s))
	    val cal_first = StringMap.lookup(first_dic,str(x))
	    val new_first_set = union(first_set,cal_first,[])
	    val update_first_dic = StringMap.insert(first_dic,str(s),new_first_set)
	  in
		if StringMap.lookup (null,str(x))
		then each_prod(s,xs,update_first_dic)
		else update_first_dic
	  end;

	fun iter_prod_list ( x, [], first_dic) = first_dic
		|iter_prod_list ( x, y::ys, first_dic ) = 
		let
			val new_dict = each_prod ( x, y, first_dic)
		in
			iter_prod_list( x, ys, new_dict ) 
		end

		

	(*this function takes non terminal and  their production dictionary as input and for each production it tries
	to find out the first of that non terminal and stores it in dictionary*)
	fun find_first ([],prod_dic,first_dic) = first_dic
		|find_first (x::xs,prod_dic,first_dic) = 
		let
			val prod_list = StringMap.lookup(prod_dic,str(x))
		in
			find_first(xs,prod_dic,iter_prod_list(x,prod_list,first_dic))
		end

	fun is_same_1 ( [], new_dict, dic ) = true
		|is_same_1 ( x::xs, new_dict, dic ) = 
		let
			val a = StringMap.lookup( dic, str(x) )
			val b = StringMap.lookup( new_dict, str(x) )
		in
			if ( areEq ( a ,b ) )
			then is_same_1 ( xs, new_dict, dic )
			else false
		end



	(*this function iter the grammar again and again to update the first dictionary*)
	fun update_first_dic(nt_list,prod_dic,first_dic) =
		let
		 	val new_dict = find_first ( nt_list,prod_dic,first_dic )
		 in
			if is_same1(nt_list,new_dict,first_dic)
			then new_dict
			else update_first_dic(nt_list,prod_dic,new_dict)
		 end 

	fun driver_to_find_first (Gram(a,b)) =
		let
			val ter = iter_grammar_ter(a,[]) 
			val nt_list = create_ntlist(a,[]);
			val symbol_rule_dict = driver_to_find_prod (a)
			val first1 = init_terminal(ter,StringMap.empty);
			val first = init_non_terminal(nt_list,first1);
		in
			update_first_dic(nt_list,symbol_rule_dict,first)
		end

	val first = driver_to_find_first(grammar1);


	(*find follow*)
	fun init_follow ( [], follow_dic:symbol list StringMap.map ) = follow_dic
		|init_follow ( x::xs, follow_dic ) = init_follow ( xs, StringMap.insert ( follow_dic, str(x), []) ) 

	
	fun update_follow_1 ( a, x, follow_dic ) =
		let
			val set1 = StringMap.lookup( follow_dic, str(x) )
			val set2 = StringMap.lookup( follow_dic, str(a) )
			val set = union ( set1, set2, [])
		in
			StringMap.insert ( follow_dic, str(x), set )
		end


	fun update_follow_2  ( x, [], follow_dic ) = follow_dic
		|update_follow_2 ( x, y::ys, follow_dic) = 
		let
			val set1 = StringMap.lookup ( follow_dic, str(x) )
			val set2 = StringMap.lookup ( first, str(y) )
			val set = union ( set1, set2, [] )
		in
			StringMap.insert ( follow_dic, str(x), set )
		end


	fun each_symbol ( a, x, [], follow_dic ) = update_follow_1 ( a, x, follow_dic )
		|each_symbol ( a, x, y::ys, follow_dic ) =
		if ( StringMap.lookup ( null, str(y) )  )
		then each_symbol ( a, x, ys, follow_dic )
		else update_follow_2 ( x, y::ys, follow_dic)


	fun each_prod_follow ( a, [], follow_dic ) = follow_dic
		|each_prod_follow ( a, x::xs, follow_dic ) =  each_prod_follow ( a, xs, each_symbol ( a, x, xs, follow_dic ) ) 
		


	fun iter_grammar_follow ( [], follow_dic ) = follow_dic
		|iter_grammar_follow (Rule(a,b)::ys, follow_dic ) = iter_grammar_follow ( ys, each_prod_follow ( a, b, follow_dic ) )

	fun update_follow_dic(set, a, follow_dic) =
		let
		 	val new_dict = iter_grammar_follow ( a, follow_dic )
		 in
			if is_same_1(set,new_dict,follow_dic)
			then new_dict
			else update_follow_dic( set, a, new_dict)
		 end 

	fun driver_to_find_follow (Gram(a,b)) = 
		let
			val nt_list = create_ntlist(a,[])
			val ter = iter_grammar_ter(a,[])
			val set = union ( nt_list, ter, [] )
			val follow_dic = init_follow ( set, StringMap.empty )
			val follow_dic_up = StringMap.insert( follow_dic, str(b), [Nonterminal("$")] )			
		in
			update_follow_dic ( set, a, follow_dic_up )
		end


 val follow = driver_to_find_follow(grammar1);	
 fun convert (Gram (a,b),f) = f(a,[])		
fun convert1 (Gram(a,b)) = StringMap.insert( follow_dic, str(b) ,[Nonterminal("$")] )
val nt = convert (grammar1,create_ntlist);
val ter = convert (grammar1,iter_grammar_ter);
val set = union ( nt, ter, [] )
val follow_dic_up = convert1(grammar1)
fun con (Gram(a,b)) = iter_grammar_follow(a,follow_dic_up)
val follow_dic1 = con(grammar1)

	StringMap.listItemsi(follow_dic1);
	StringMap.listItems(follow_dic1)