
fun isolate [] = []
	|isolate ( x::xs ) = x :: isolate (List.filter(fn y => y <> x) xs)


fun is_member(x,[]) = false
	|is_member(x,y::ls) =
	if ( x = y ) 
	then true 
	else is_member(x,ls)


fun set_difference ( [], ys, k ) = k
	|set_difference ( x::xs, ys, k ) =
	if ( is_member ( x,ys ) ) then set_difference ( xs, ys, k )
	else set_difference ( xs, ys, k @ [x] )



structure IntKey =
struct
	type ord_key = int
	val compare = Int.compare
end

structure IntMap = ListMapFn (IntKey:ORD_KEY):ORD_MAP

structure IntSet = ListSetFn(IntKey:ORD_KEY):ORD_SET





signature Inst =
sig
    type inst
    val useSet : inst -> AtomSet.set (* get the use set of an instruction *)
    val defSet : inst -> AtomSet.set (* get the def set of an instruction *)
end

structure Instruction : Inst =
struct
	(* Body *)
	type inst =  string list * string list
	fun useSet (def,use) = AtomSet.fromList ( List.map (Atom.atom) (use) )
	fun defSet (def,use) = AtomSet.fromList ( List.map (Atom.atom )(def) )
end


structure InstList : Inst = 
struct
	(* Body *)

	type inst = (string list * string list) list

	fun calgenkill ( g1,k1,g2,k2 ) =  
			let 
				val diff = AtomSet.difference (g2,k1)
				val g = AtomSet.union( g1, diff ) 
				val k = AtomSet.union ( k1, k2 )
			in
				(g,k)
			end

	fun update_genkill ([],g1,k1) = (g1,k1)
		|update_genkill ((a,b)::xs,g1,k1) =
		let
		 	val k2 = AtomSet.fromList ( List.map (Atom.atom) (a) )
		 	val g2 = AtomSet.fromList ( List.map (Atom.atom) (b) )
		 	val (g,k) = calgenkill (g1,k1,g2,k2)
		 in
		 	update_genkill (xs,g,k)
		 end 
	
	fun driver (xs) = update_genkill(xs,AtomSet.empty,AtomSet.empty) 

	fun useSet (xs) = #1 (driver(xs))
	fun defSet (xs) = #2 (driver(xs))

end
(*
g1 = useset(1)
g2 = useset(2)
k1 = defset(1)
k2 = defset(2)
g3 = useset(3)
g3 = defset(3)

g = g1 union g2/k1
k = k1 union k2

g1->g
g2->g3

k1->k
k2->k3

g = (g1 union g2/k1) union  g3/(k1 union k2)
k = (k1 union k2) union k3

*)



functor CalInOut (X:Inst) = 
struct
	(* Body *)

	fun isequal([],new_dic,dic) = true
		|isequal(x::xs,new_dic,dic) =
			let
				val set = IntMap.lookup (dic,x)
				val new_set = IntMap.lookup (new_dic,x)
			in
				if AtomSet.equal(set,new_set)
				then isequal(xs,new_dic,dic)
				else false
			end


	fun set_dic ( x, instr,use_dic,def_dic) = 
		let
			val useset = X.useSet (instr)
			val defset = X.defSet (instr)
			val use_dic_new = IntMap.insert ( use_dic, x, useset )
			val def_dic_new = IntMap.insert ( def_dic, x, defset )
		in
			(use_dic_new,def_dic_new)
		end

	fun conAtom ([],inst_dic,use_dic,def_dic) = (use_dic,def_dic) 
		|conAtom (x::xs,inst_dic,use_dic,def_dic) = 
			let
				val instr = IntMap.lookup(inst_dic,x)
				val (use_dic_new,def_dic_new) = set_dic ( x, instr,use_dic,def_dic)
			in
				conAtom(xs,inst_dic,use_dic_new,def_dic_new)
			end

  	fun init_inout (xs) =
		let
			fun init_dic ( [], a ) = a
				|init_dic ( x::xs, a) = init_dic ( xs, IntMap.insert ( a, x, AtomSet.empty ) ) 
			val in_dic = init_dic ( xs, IntMap.empty )
			val out_dic = init_dic ( xs, IntMap.empty )
		in
			(in_dic,out_dic)
		end


	fun dounion ([],in_dic,k) = k
	|dounion (x::xs,in_dic,k) =
		let
			val inset = IntMap.lookup (in_dic,x)
			val k_up = AtomSet.union (inset,k)
		in
			dounion (xs,in_dic,k_up)
		end


	fun update_dic ([],succ,in_dic,out_dic,use_dic,def_dic) = (in_dic,out_dic)
	|update_dic (x::xs,succ,in_dic,out_dic,use_dic,def_dic) =
		let
			val succset = IntMap.lookup(succ,x)
			val succ_list = IntSet.listItems (succset)
			val outset = IntMap.lookup(out_dic,x)
			val defset = IntMap.lookup (def_dic,x)
			val useset = IntMap.lookup (use_dic,x)
			val diff = AtomSet.difference (outset,defset)
			val inset_up = AtomSet.union (useset,diff)
			val in_dic_up = IntMap.insert (in_dic,x,inset_up)
			val outset_up = dounion(succ_list,in_dic,AtomSet.empty)
			val out_dic_up = IntMap.insert (out_dic,x,outset_up)
		in
			update_dic (xs,succ,in_dic_up,out_dic_up,use_dic,def_dic)
		end



	

	fun fixedptcal (node,succ,in_dic,out_dic,use_dic,def_dic) =
		let
			val (in_dic_new,out_dic_new) = update_dic (node,succ,in_dic,out_dic,use_dic,def_dic)
		in
			if ( isequal(node,in_dic_new,in_dic) andalso isequal(node,out_dic_new,out_dic) )
			then (in_dic_new,out_dic_new)
			else fixedptcal (node,succ,in_dic_new,out_dic_new,use_dic,def_dic)
		end

	fun driver_cal (node,succ,use_dic,def_dic) = 
		let
			val (in_dic,out_dic) = init_inout(node)
		in
			fixedptcal(node,succ,in_dic,out_dic,use_dic,def_dic)
		end

end






(*--------------------------end of 1st and 2nd part*)
signature Graph = 
sig
	(*eqtype node*)
	type graph     (* mutable variant of a graph *)
	type node
	val newNode : graph * node * (string list * string list) -> graph
	val addEdge : graph * node * node -> graph
	val nodes   : graph -> node list
	val suc_dic : graph -> IntSet.set IntMap.map
	val suc     : graph*node -> node list
	val pred    : graph*node -> node list
	val inst    : graph -> (string list * string list) IntMap.map
	val empty   : unit -> graph
	val driver  : graph -> int list list
end

structure GraphStr : Graph =
struct
	(* Body *)

	type graph = (  node list *  IntSet.set IntMap.map  *  IntSet.set IntMap.map * (string list * string list) IntMap.map )

	type node = int

	fun add_dic_edge ( b, x, y ) = 
		let
			val set = IntMap.lookup ( b, x )
			val new_set = IntSet.add ( set, y )
			val new_dic = IntMap.insert ( b, x, new_set )
		in
			new_dic
		end

		 

	fun newNode ( (a,b,c,d):graph, x, (xs,ys) ) = 
		let
			val a_new = a @ [x] 
			val b_new = IntMap.insert ( b, x, IntSet.empty )
			val c_new = IntMap.insert ( c, x, IntSet.empty )
			val d_new = IntMap.insert ( d, x, (xs,ys) )
		in
			( a_new, b_new, c_new,d_new )
		end


	fun addEdge ( (a,b,c,d):graph, x, y ) =
		let
		 	val b = add_dic_edge ( b, y, x )
		 	val c = add_dic_edge ( c, x, y )
		in
			( a, b, c, d )
		end

	fun nodes( (a,b,c, d):graph ) = a

	fun inst ( (a,b,c,d):graph ) = d

	fun suc_dic ( (a,b,c,d):graph ) = c

	fun suc ( (a,b,c, d ):graph ,x ) = IntSet.listItems ( IntMap.lookup ( b, x ) )
	fun pred ( (a,b,c, d):graph ,x ) = IntSet.listItems ( IntMap.lookup ( c, x ) )
	fun empty ( ) = ([],IntMap.empty,IntMap.empty,IntMap.empty)



	fun find_pred_suc (x,b,c) =
		let
			val pred_num = IntSet.numItems ( IntMap.lookup ( b, x ) )
		 	val pred_list = IntSet.listItems ( IntMap.lookup ( b, x ) )
		 	val suc_num = IntSet.numItems ( IntMap.lookup ( c, x ) )
		 	val suc_list = IntSet.listItems ( IntMap.lookup ( c, x ) )
		in
			(pred_num,pred_list,suc_num,suc_list)
		end



	fun check_entry ([], b, c, k ) = k
		|check_entry ( x::xs, b, c, k ) =
		let
			val (pred_num,pred_list,suc_num,suc_list) = find_pred_suc (x,b,c)	 	
		in
			if ( pred_num = 1 andalso suc_num < 2 ) 
		 	then 
		 		if ( is_member ( x, k ) )
		 		then k
		 		else check_entry ( pred_list, b, c, [x] @ k )
		 	else
		 		if ( suc_num > 1 ) then k
		 		else [x] @ k
		end 




	fun check_exit ( [], b, c, k ) = k
		|check_exit ( x::xs, b, c, k ) = 
		let
			val (pred_num,pred_list,suc_num,suc_list) = find_pred_suc (x,b,c) 
		in
			if ( suc_num = 1 andalso pred_num < 2 ) 
			then
				if ( is_member ( x, k ) )
				then k @ [x]
				else check_exit ( suc_list, b, c, k @ [x] ) 
	 		else
	 			if ( pred_num > 1 ) then k
	 			else k @ [x] 
		end







	fun leader (x, b, c) =
		let
			val (pred_num,pred_list,suc_num,suc_list) = find_pred_suc (x,b,c)
		in
			if ( not (pred_num = 1) andalso not (suc_num = 1) )
			then [x]
			else
				if (pred_num = 1 andalso suc_num = 1)
				then check_entry ( pred_list, b, c, []) @ [x] @ check_exit (suc_list, b, c, []) 
				else
					if (pred_num = 1)
					then check_entry ( pred_list, b, c, []) @ [x]
					else [x] @ check_exit (suc_list, b, c, [])
		end



	fun find_basic_node ( [], b, c, ker ) = ker
		|find_basic_node ( x::xs, b, c, ker ) =
		let
			val basic_block =  isolate ( leader (x,b,c) )
			val newlist = set_difference ( xs, basic_block, [] )
			val ker_up = ker @ [basic_block]
		in
			find_basic_node ( newlist, b, c, ker_up )
		end

	fun driver (a,b,c,d) = find_basic_node (a,b,c, [])



end



val graph1 = GraphStr.empty ();
val graph1 = GraphStr.newNode( graph1, 1, ( ["a"], [] ) )
val graph1 = GraphStr.newNode( graph1, 2, ( ["b"], ["a"] ) )
val graph1 = GraphStr.newNode( graph1, 3, ( ["c"], ["c","b"] ) )
val graph1 = GraphStr.newNode( graph1, 4, ( ["a"], ["b"] ) )
val graph1 = GraphStr.newNode( graph1, 5, ( [], ["a"] ) )
val graph1 = GraphStr.newNode( graph1, 6, ( [], ["c"] ) )

val graph1 = GraphStr.addEdge (graph1, 1, 2)
val graph1 = GraphStr.addEdge (graph1, 2, 3)
val graph1 = GraphStr.addEdge (graph1, 3, 4)
val graph1 = GraphStr.addEdge (graph1, 4, 5)
val graph1 = GraphStr.addEdge (graph1, 5, 2)
val graph1 = GraphStr.addEdge (graph1, 5, 6)


structure inst_cal = CalInOut(Instruction:Inst)
val node = GraphStr.nodes(graph1)
val succ = GraphStr.suc_dic (graph1)
val inst_dic = GraphStr.inst(graph1)
val (use_dic,def_dic) = inst_cal.conAtom ( node, inst_dic, IntMap.empty,IntMap.empty )
val (inIntMap,outIntMap) = inst_cal.driver_cal(node,succ,use_dic,def_dic)
List.map(Atom.toString) (AtomSet.listItems(IntMap.lookup(outIntMap,6)));
List.map(Atom.toString) (AtomSet.listItems(IntMap.lookup(inIntMap,6)));



val basic_list = GraphStr.driver(graph1);



fun basic_block_dic_fn ( [], k_dic ) = k_dic
	|basic_block_dic_fn ( x::xs, k_dic ) = basic_block_dic_fn ( xs,IntMap.insert ( k_dic, List.hd(x), x ) )



fun get_instr_list ( [], inst_dic, k_list) = k_list
	|get_instr_list ( y::ys, inst_dic, k_list) = 
		let
			val inst = IntMap.lookup (inst_dic,y)
		in
			get_instr_list ( ys, inst_dic, k_list @ [inst] )
		end

fun insrt (x,y,inst_dic,k_dic) = IntMap.insert ( k_dic, y, get_instr_list( x, inst_dic, [] ) )


fun basic_inst_dic_fn ( [], basic_dic, inst_dic, k_dic ) = k_dic
	|basic_inst_dic_fn ( x::xs, basic_dic, inst_dic, k_dic ) = basic_inst_dic_fn( xs, basic_dic, inst_dic, insrt ( IntMap.lookup(basic_dic,x) , x, inst_dic, k_dic ) )   




fun find_basic_succ ([], check_set, succ, k_dic ) = k_dic
	|find_basic_succ (x::xs, check_set, succ, k_dic ) = 
	let
		val set = IntMap.lookup(succ,x)
		val suc_set = IntSet.intersection ( check_set, set)
		val k_dic_new = IntMap.insert ( k_dic, x, suc_set )
	in
		find_basic_succ (xs,check_set,succ, k_dic_new )
	end



fun create_graph ( g:graph) = 
	let
		val inst_dic = GraphStr.inst(g)
		val succ = GraphStr.suc_dic (g)
		val basic_list = GraphStr.driver(g)
		val basic_dic = basic_block_dic_fn (basic_list, IntMap.empty)
		val node_list = IntMap.listKeys( basic_dic )
		val check_set = IntSet.fromList (node_list)
		val basic_inst_dic = basic_inst_dic_fn ( basic_list, basic_dic, inst_dic, IntMap.empty )
		structure inst_cal = CalInOut(InstList:Inst)
		val (use_dic,def_dic) = inst_cal.conAtom ( node_list, basic_inst_dic, IntMap.empty,IntMap.empty )
		val basic_succ = find_basic_succ (node_list, check_set,succ, IntMap.empty )	
	in
		(inIntMap,outIntMap) = inst_cal.driver_cal(node_list, basic_succ, use_dic,def_dic)
	end

































 


(*val useIntMap = IntMap.empty;
val useIntMap = IntMap.insert(useIntMap,1,Instruction.useSet(["a"],[]))
val useIntMap = IntMap.insert(useIntMap,2,Instruction.useSet(["b"],["a"]))
val useIntMap = IntMap.insert(useIntMap,3,Instruction.useSet(["c"],["c","b"]))
val useIntMap = IntMap.insert(useIntMap,4,Instruction.useSet(["a"],["b"]))
val useIntMap = IntMap.insert(useIntMap,5,Instruction.useSet([],["a"]))
val useIntMap = IntMap.insert(useIntMap,6,Instruction.useSet([],["c"]))
val defIntMap = IntMap.empty;
val defIntMap = IntMap.insert(defIntMap,1,Instruction.defSet(["a"],[]))
val defIntMap = IntMap.insert(defIntMap,2,Instruction.defSet(["b"],["a"]))
val defIntMap = IntMap.insert(defIntMap,3,Instruction.defSet(["c"],["c","b"]))
val defIntMap = IntMap.insert(defIntMap,4,Instruction.defSet(["a"],["b"]))
val defIntMap = IntMap.insert(defIntMap,5,Instruction.defSet([],["a"]))
val defIntMap = IntMap.insert(defIntMap,6,Instruction.defSet([],["c"]))
val succ = IntMap.empty;
val succ = IntMap.insert(succ,1,[2])
val succ = IntMap.insert(succ,2,[3])
val succ = IntMap.insert(succ,3,[4])
val succ = IntMap.insert(succ,4,[5])
val succ = IntMap.insert(succ,5,[2,6])
val succ = IntMap.insert(succ,6,[])

structure inst_cal = CalInOut(Instruction:Inst)
val (inIntMap,outIntMap) = inst_cal.driver_cal([1,2,3,4,5,6],succ,useIntMap,defIntMap);

List.map(Atom.toString) (AtomSet.listItems(IntMap.lookup(outIntMap,6)));
List.map(Atom.toString) (AtomSet.listItems(IntMap.lookup(inIntMap,6)));

val map = IntMap.empty;
val map = IntMap.insert(useIntMap,[1,2], )
val map = IntMap.insert(useIntMap,3,(["w"],["w","z"]))
val map = IntMap.insert(useIntMap,4,(["z"],["x","y"]))

val a = InstList.driver( [ ( ["x"],["y","z"] ),( ["x"],["w","z"] ) ] )

val gen = List.map ( Atom.toString)  ( AtomSet.listItems (#1 (a) ) ) 
val kill = List.map ( Atom.toString)  ( AtomSet.listItems (#2 (a) ) ) 

val a = InstList.driver( [ ( ["w"],["w","z"] ) ] )
val gen = List.map ( Atom.toString)  ( AtomSet.listItems (#1 (a) ) ) 
val kill = List.map ( Atom.toString)  ( AtomSet.listItems (#2 (a) ) ) 


val a = InstList.driver( [ ( ["z"],["x","y"] ) ] )
val gen = List.map ( Atom.toString)  ( AtomSet.listItems (#1 (a) ) ) 
val kill = List.map ( Atom.toString)  ( AtomSet.listItems (#2 (a) ) ) 

*)





