# This is repoitory of lab assignment done in sem 5 for compiler

There are 3 sml files
1-fistfollow.sml computes the first and follow of a given cfg
2-basicblock.sml computes the basic block of a given graph from its predecessor and successor
3-liveness analysis it does a liveness analysis.There are 2 type of analysis. First it finds the in and out of each instruction using use and def.The second part uses the previous basicblock algorithm to generate the basic block and then generates the kill and gen set uses them to do the liveness analysis of basic block.
